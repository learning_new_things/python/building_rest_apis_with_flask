"""From Listing 1.1- Basic Flask Application"""

from flask import Flask

# Initialize the class Flask and call the current module
app = Flask(__name__)

"""The route() function of the Flask class is a decorator, 
which tells the application which URL should call the associated function.

app.route(rule, options)
"""
@app.route('/')  # The / is bound with the function, so the function runs on home
def hello_world():
    return 'Hello, from Flask!'


"""the run() method of Flask class runs the application 
on the local development server.

app.run(host, port, debug, options)
"""

if __name__ == '__main__':
    app.run()

"""Run Options:
Sr.No.	Parameters & Description
1	
host

Hostname to listen on. Defaults to 127.0.0.1 (localhost). Set to ‘0.0.0.0’ 
to have server available externally

2	
port

Defaults to 5000

3	
debug

Defaults to false. If set to true, provides a debug information

4	
options

To be forwarded to underlying Werkzeug server.
"""
